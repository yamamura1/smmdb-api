# SMMDB-API
This is a ESM NodeJS module for interacting with the SMMDB (Super Mario Maker) API.
```js
npm install smmdb-api
```

## Usage
Since this is class based, you will have to initiate it with `new`, with your API key as the argument if you have it.
Callbacks are optional in these classes, and not providing one will return a promise you will have to await/.then. They are the typical `(error, stats)` function classes.

### getStats(callback)
Gets SMMDB statistics. An object will be returned provided an error didn't occur

* `courses`: Number of SMM courses (total 3DS and WiiU)
* `courses64`: Number of SM64M (Super Mario 64 Maker) levels
* `accounts`: Number of accounts

## [searchCourses({parameters}, callback)](https://github.com/Tarnadas/smmdb/blob/master/docs/API.md#receive-course-list)
Search SMMDB courses. The first argument will be your parameters.
It will return an array of courses.

## toggleStar(courseId, callback)
Gives/Removes a star on an SMMDB course depending on if it was stared before or not. This method requires an API key.
An object with the course's information will be returned.

## downloadCourse(courseId, target, callback)
Downloads an SMMDB course by its ID to a specified location.

## uploadCourse(buffer, callback)
Uploads an SMMDB course from a buffer. This method requires an API key.
This returns information on the uploaded course
